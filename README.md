# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

 * `LISTEN_PORT` - Port to listen on (default: `8000`)
 * `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
 * `APP_PORT` - Port of the app to forward requests to (default: `9000`)

### AWS resources

* ECR repository
* IAM Policy RecipeAppApi-ProxyCIPushECR
* IAM user recipe-app-api-proxy-ci
* push again to trigger the pipeline
* push again
